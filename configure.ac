dnl Process this file with autoconf to produce a configure script.
AC_INIT(cygwin-x-doc, m4_esyscmd([echo -n `cat version`]))
AC_CONFIG_SRCDIR([common/entities.xml])
AM_INIT_AUTOMAKE([dist-bzip2 no-dist-gzip foreign -Wall])
AC_PROG_LN_S

# Checks for programs.

dnl --with-docdir
AC_ARG_WITH(docdir,
    AC_HELP_STRING( 
      [--with-docdir=DIR], [general documentation (DATADIR/doc/cygwin-x-doc)]),
    [docdir="$withval"],
    [docdir='${datadir}/doc/cygwin-x-doc'])
AC_SUBST(docdir)

# dsssl processor
AC_CHECK_PROGS([JADE],[jade openjade])
test -z "$JADE" && AC_MSG_ERROR([No dsssl tool found.  Cannot build documentation.])

# sgml validator
AC_CHECK_PROGS([NSGMLS],[nsgmls onsgmls])
test -z "$NSGMLS" && AC_MSG_WARN([No sgml validator found.  Cannot validate sgml content.])

AC_ARG_ENABLE(hardcopy, 
    AC_HELP_STRING( 
      [--disable-hardcopy], [disable building of hardcopy documentation]),
    [build_hardcopy=$enableval],[build_hardcopy=auto]
    )    

if test x$build_hardcopy != xno; then
AC_CHECK_PROGS([JADETEX],[jadetex])

if test -z "$JADETEX" ; then
    AC_MSG_WARN([jadetex not found. Cannot build hardcopy documentation.])
    build_hardcopy=no
fi
fi

AC_CHECK_PROGS([LYNX],[lynx])
test -z "$LYNX" && AC_MSG_ERROR([No html-to-text tool found.  Cannot build documentation.])

AC_CHECK_PROGS([CONVERT],[convert])
dnl do not try to use the wrong convert!
case `which $CONVERT` in
    *system32*)
        CONVERT=
        ;;
esac
test -z "$CONVERT" && AC_MSG_ERROR([ImageMagick convert not found.  Cannot build documentation.])

dnl get the docbook.dsl file from the commandline  
AC_ARG_WITH(docbook-dsl,
    AC_HELP_STRING( 
      [--with-docbook-dsl=FILE], [docbook.dsl stylesheet]),
    [
        docbook_dsl_file="$withval"
        docbook_dsl_files=""
    ],
    [
        docbook_dsl_file=""
        docbook_dsl_files=" \
      /usr/share/sgml/docbook/dsssl-stylesheets/print/docbook.dsl \
      /usr/share/sgml/docbook-dsssl/print/docbook.dsl \
      /usr/share/sgml/docbook/stylesheet/dsssl/modular/print/docbook.dsl \
      "
    ])

dnl check for installed docbook.dsl
if test x$build_hardcopy != xno; then
  for file in "$docbook_dsl_file" $docbook_dsl_files; do 
    test -z "$file" && continue
    AC_CHECK_FILE($file, 
      [
        build_hardcopy=yes
        docbook_dsl="$file"
        break
      ])  
  done  
  if test x$docbook_dsl == x; then
    if test x$build_hardcopy == xyes; then
      AC_MSG_ERROR([stylesheet not found. Cannot build hardcopy documentation.])
    else
      AC_MSG_WARN([stylesheet not found. Build of hardcopy documentation disabled.])
    fi
  else
    AC_MSG_NOTICE([build of hardcopy documentation enabled.])
    DOCBOOK_DSL="$docbook_dsl"
  fi
else
  AC_MSG_NOTICE([build of hardcopy documentation disabled.])
fi

dnl export settings to makefiles
AC_SUBST(DOCBOOK_DSL)
AM_CONDITIONAL(BUILD_HARDCOPY, test x$build_hardcopy = xyes)

AC_CONFIG_FILES([Makefile 
		cg/Makefile 
		faq/Makefile 
		ug/Makefile 
		stylesheets/Makefile 
		ug/figures/Makefile 
		common/Makefile])
AC_OUTPUT
