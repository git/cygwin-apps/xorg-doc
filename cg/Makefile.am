MAX_TEX_RECURSION=4

XML_DECL=$(srcdir)/../stylesheets/xml.dcl
JADE_INCLUDE=-D $(builddir)/../common/

HTML_SS=$(srcdir)/../stylesheets/website.dsl
PRINT_SS=@DOCBOOK_DSL@

ENTITIES=$(srcdir)/../common/entities.xml
FDL=$(srcdir)/../common/fdl.xml

export SP_ENCODING=utf-8

## Source files

cg_xml_source = cygwin-x-cg.xml
extra_cg_xml_source = biblio.xml \
	documentation.xml \
	documentation-build.xml \
	documentation-distribution.xml \
	documentation-docbook.xml \
	documentation-obtaining-source.xml \
	glossary.xml \
	overview.xml \
	programming.xml \
	programming-tree-layout.xml \
	programming-server-architecture.xml \
	programming-prerequisites.xml \
	programming-obtaining-source.xml \
	programming-native.xml \
	programming-cross.xml \
	programming-patches.xml \
	programming-distribution.xml \
	programming-reference.xml \
	website.xml \
	appendix-cross.xml

## Files generated by HTML build

extra_cg_html = \
	biblio.html \
	colophon.html \
	cross.html \
	cross-building-binutils.html \
	cross-building-gcc.html \
	cross-obtaining-cygwin-headers-and-libs.html \
	docs-build.html \
	docs-distribution.html \
	docs-docbook.html \
	docs-obtaining-source.html \
	documentation.html \
	gfdl.html \
	glossary.html \
	overview.html \
	prog-build-cross.html \
	prog-build-native.html \
	prog-build-prerequisites.html \
	prog-distribution.html \
	prog-obtaining-source.html \
	prog-patches.html \
	programming.html \
	prog-reference.html \
	prog-server-architecture.html \
	prog-tree-layout.html \
	website.html

## Files generated by other builds

if BUILD_HARDCOPY  
extra_cg_ps = cygwin-x-cg.ps
extra_cg_ps_gz = cygwin-x-cg.ps.gz
extra_cg_pdf = cygwin-x-cg.pdf
extra_cg_dvi = cygwin-x-cg.dvi
extra_cg_tex = cygwin-x-cg.tex
extra_cg_rtf = cygwin-x-cg.rtf
else
extra_cg_ps =
extra_cg_ps_gz =
extra_cg_pdf =
extra_cg_dvi =
extra_cg_tex =
extra_cg_rtf =
endif
extra_cg_txt = cygwin-x-cg.txt

cgdir = $(docdir)/cg
cg_DATA = cygwin-x-cg.html $(extra_cg_html) $(extra_cg_ps_gz) \
	$(extra_cg_pdf) $(extra_cg_txt) $(extra_cg_rtf)
EXTRA_DIST = $(cg_xml_source) $(extra_cg_xml_source)

## Dependency rules follow

cygwin-x-cg.html $(extra_cg_html): $(HTML_SS) $(cg_xml_source) \
	$(extra_cg_xml_source) $(ENTITIES) $(FDL)

$(extra_cg_tex): $(cg_xml_source) $(extra_cg_xml_source) $(ENTITIES) $(FDL)

$(extra_cg_dvi): $(extra_cg_tex)

$(extra_cg_ps): $(extra_cg_dvi)

$(extra_cg_ps_gz): $(extra_cg_ps)

$(extra_cg_pdf): $(extra_cg_dvi)

$(extra_cg_txt): $(cg_xml_source) $(extra_cg_xml_source) $(ENTITIES) $(FDL)

$(extra_cg_rtf): $(cg_xml_source) $(extra_cg_xml_source) $(ENTITIES) $(FDL)

## General rules follow

clean-local:
	rm -f *.htm *.html *.dvi *.tex *.ps *.pdf \
	*.txt *.log *.aux *.rtf *.gz *.out

validate:
	$(NSGMLS) -wmin-tag -s $(srcdir)/$(cg_xml_source)

## Support rules follow

$(extra_cg_tex):
	$(JADE) -t tex -V tex-backend -d $(PRINT_SS)  $(JADE_INCLUDE) -o $@ $(XML_DECL) $(srcdir)/$(cg_xml_source)

## Trick from Adam Di Carlo <adam@onshore.com> to recurse jadetex 
## "just enough".
$(extra_cg_dvi):
	-cp -pf prior.aux pprior.aux
	-cp -pf $(shell basename $< .tex).aux prior.aux
	$(JADETEX) $<
	if ! cmp $(shell basename $< .tex).aux prior.aux &&		\
	   ! cmp $(shell basename $< .tex).aux pprior.aux &&		\
	   expr $(MAKELEVEL) '<' $(MAX_TEX_RECURSION); then		\
		rm -f $@						;\
		$(MAKE) $@						;\
	fi
	rm -f prior.aux pprior.aux

## Target rules follow

$(extra_cg_ps):
	dvips -f $< > $@

$(extra_cg_ps_gz):
	cat $< | gzip -f > $@

$(extra_cg_pdf):
	dvipdf -f $< > $@

cygwin-x-cg.html:
	$(NSGMLS) -wmin-tag -s $<
	$(JADE) -t xml -ihtml -d $(HTML_SS) $(JADE_INCLUDE) -o $@ $(XML_DECL) $(srcdir)/$(cg_xml_source)

$(extra_cg_txt):
	$(JADE) -t xml -V nochunks -d $(HTML_SS) $(JADE_INCLUDE) $(XML_DECL) $(srcdir)/$(cg_xml_source) > dump.html
	$(LYNX) -force_html -dump -nolist dump.html > $@
	-rm -f dump.html

$(extra_cg_rtf):
	$(JADE) -t rtf -d $(PRINT_SS) $(JADE_INCLUDE) -o $@ $(XML_DECL) $(srcdir)/$(cg_xml_source)
