<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                    "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd"
[                  

<!-- Include the common entities -->
<!ENTITY % common SYSTEM "../common/entities.xml"> %common;

<!-- System entities -->
<!ENTITY overview SYSTEM "overview.xml">
<!ENTITY programming SYSTEM "programming.xml">
<!ENTITY programming-tree-layout SYSTEM
"programming-tree-layout.xml">
<!ENTITY programming-server-architecture SYSTEM
"programming-server-architecture.xml">
<!ENTITY programming-prerequisites SYSTEM "programming-prerequisites.xml">
<!ENTITY programming-obtaining-source SYSTEM
"programming-obtaining-source.xml">
<!ENTITY programming-native SYSTEM "programming-native.xml">
<!ENTITY programming-cross SYSTEM "programming-cross.xml">
<!ENTITY programming-patches SYSTEM "programming-patches.xml">
<!ENTITY programming-distribution SYSTEM "programming-distribution.xml">
<!ENTITY programming-reference SYSTEM "programming-reference.xml">
<!ENTITY documentation SYSTEM "documentation.xml">
<!ENTITY documentation-obtaining-source SYSTEM
"documentation-obtaining-source.xml">
<!ENTITY documentation-docbook SYSTEM "documentation-docbook.xml">
<!ENTITY documentation-build SYSTEM "documentation-build.xml">
<!ENTITY documentation-distribution SYSTEM
"documentation-distribution.xml">
<!ENTITY website SYSTEM "website.xml">
<!ENTITY building-cross SYSTEM "appendix-cross.xml">
<!ENTITY biblio SYSTEM "biblio.xml">
<!ENTITY glossary SYSTEM "glossary.xml">

<!-- Project entities -->
<!ENTITY project-path-devel "~/xserver">
<!ENTITY project-path-stagingdir "/stagingdir">
<!ENTITY project-path-bindistdir "/bindistdir">
<!ENTITY project-path-commondistdir "/commondist">
<!ENTITY project-path-testdir "/testdir">

<!-- Programming title entities -->
<!ENTITY title-prog-tree-layout "Source Code Tree Layout">
<!ENTITY title-prog-server-architecture "&project; X Server
Architecture">
<!ENTITY title-prog-server-architecture-privates "Server Privates">
<!ENTITY title-prog-server-architecture-privates-structs "Structures">
<!ENTITY title-prog-server-architecture-privates-macros "Macros">
<!ENTITY title-prog-server-architecture-engines "Engine System">
<!ENTITY title-prog-server-architecture-engines-shadow "Shadow FB Engines">
<!ENTITY title-prog-server-architecture-engines-native "Native GDI
Engine">
<!ENTITY title-prog-server-architecture-engines-primary "Primary FB Engine">
<!ENTITY title-prog-server-architecture-input "User Input">
<!ENTITY title-prog-server-architecture-input-keyboard "Keyboard">
<!ENTITY title-prog-server-architecture-input-mouse "Mouse">
<!ENTITY title-prog-server-architecture-input-mouse-wheel "Wheel">
<!ENTITY title-prog-server-architecture-input-mouse-motion "Motion">
<!ENTITY title-prog-server-architecture-input-mouse-buttons "Buttons">
<!ENTITY title-prog-server-architecture-input-mouse-3be "Three Button
Emulation">
<!ENTITY title-prog-prereqs "Prerequisites for Building the Source Code">
<!ENTITY title-prog-obtaining-source "Obtaining the Source Code">
<!ENTITY title-prog-build-native "Native Compiling">
<!ENTITY title-prog-build-cross "Cross Compiling">
<!ENTITY title-prog-cross-obtaining-compiler-source "Obtaining &binutils; and &gcc; Source">
<!ENTITY title-prog-cross-obtaining-cygwin-headers-and-libs "Obtaining &cygwin; Headers and Libs">
<!ENTITY title-prog-cross-building-binutils-and-gcc "Building &binutils; and &gcc;">
<!ENTITY title-prog-cross-tool-links "Creating Links for &binutils; and &gcc;">
<!ENTITY title-prog-cross-build "Building &project;">
<!ENTITY title-prog-patches "Contributing Patches">
<!ENTITY title-prog-distribution "Packaging a &project; Distribution">
<!ENTITY title-prog-reference "Reference Documentation">

<!-- Documentation title entities -->
<!ENTITY title-docs "Documentation">
<!ENTITY title-docs-obtaining-source "Obtaining the Source Code">
<!ENTITY title-docs-docbook "Setting Up a DocBook Build Environment">
<!ENTITY title-docs-build "Building the Documentation">
<!ENTITY title-docs-distribution "Packaging a Documentation
Distribution">

<!-- Website title entities -->
<!ENTITY title-web "Web Site Maintenance">

<!-- Cygnus entities -->
<!ENTITY file-cygwin-lib "cygwin-lib">
<!ENTITY file-cygwin-lib-arc "&file-cygwin-lib;&ext-tgz;">
<!ENTITY file-cygwin-lib-arc-size "16 MiB">

<!ENTITY file-cygwin-include "cygwin-include">
<!ENTITY file-cygwin-include-arc "&file-cygwin-include;&ext-tgz;">
<!ENTITY file-cygwin-include-arc-size "1.5 MiB">

<!-- Cross compiler entities -->
<!ENTITY path-cross-root "/home/my_login/cygwin">
<!ENTITY path-cross-src "&path-cross-root;/src/">
<!ENTITY dir-cross-src "src">
<!ENTITY cross-prefix "i686-pc-cygwin">
<!ENTITY file-cross-links-script "cross-links.sh">
<!ENTITY path-crosscompiledir "&path-cross-root;/&cross-prefix;/bin">
<!ENTITY path-cross-lib-and-include "&path-cross-root;/&cross-prefix;/">

<!ENTITY gcc "gcc">
<!ENTITY gcc-version "3.3.1-1">
<!ENTITY dir-gcc-src "gcc-&gcc-version;">
<!ENTITY path-download-gcc "gcc/">
<!ENTITY path-gcc-src "&path-cross-src;&dir-gcc-src;">
<!ENTITY file-gcc-src "gcc-&gcc-version;-src">
<!ENTITY file-gcc-src-tar "&file-gcc-src;&ext-tar;">
<!ENTITY file-gcc-src-arc "&file-gcc-src;&ext-zipped;">
<!ENTITY file-gcc-src-arc-size "21.9 MiB">

<!ENTITY binutils "binutils">
<!ENTITY binutils-version "20030901-1">
<!ENTITY dir-binutils-src "binutils-&binutils-version;">
<!ENTITY path-download-binutils "binutils/">
<!ENTITY path-binutils-src "&path-cross-src;&dir-binutils-src;">
<!ENTITY file-binutils-src "binutils-&binutils-version;-src">
<!ENTITY file-binutils-src-tar "&file-binutils-src;&ext-tar;">
<!ENTITY file-binutils-src-arc "&file-binutils-src;&ext-zipped;">
<!ENTITY file-binutils-src-arc-size "10.5 MiB">

<!ENTITY cross-bash-prompt-open "[harold@CrossHost">
<!ENTITY cross-bash-prompt-close "]$">

<!-- Project file names -->
<!ENTITY file-documentation-source-version "1.0.0">
<!ENTITY file-documentation-source "cygwin-x-doc-&file-documentation-source-version;">
<!ENTITY file-documentation-source-arc "&file-documentation-source;&ext-zipped;">
<!ENTITY file-documentation-source-arc-size "685 KiB">
]>

<book id="cygwin-x-cg">

<bookinfo id="book-info">

<title>&project; Contributor's Guide</title>

<author>
<firstname>Harold</firstname>
<othername role="mi">L</othername>
<surname>Hunt</surname>
<lineage>II</lineage>
</author>

<author>
<firstname>Jon</firstname>
<surname>Turney</surname>
</author>

<legalnotice id="legal-notice">
<blockquote id="legal-notice-quote"><para>
Copyright (C) 2004 Harold L Hunt II.  Copyright (C) 2009-2021 Jon Turney.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, with no
Front-Cover Texts, and with no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".
</para></blockquote>
</legalnotice>

</bookinfo>

<toc></toc>

&overview;
&programming;
&documentation;
&website;
&biblio;
&glossary;
&building-cross;
&fdl;

<colophon id="colophon">
<para>
This document was produced from <ulink url="&docbook-home;">&docbook;</ulink>
source XML using <ulink url="http://openjade.sourceforge.net/">OpenJade</ulink>
and the DocBook DSSSL Stylesheets.
</para>
<para>
The production process for this instance of this document was started at &buildinfo;.
</para>
</colophon>

</book>
