<sect1 id="prog-obtaining-source">
<title>&title-prog-obtaining-source;</title>

<sect2 id="prog-source-cygwin-setup">
<title>Obtaining via Cygwin setup</title>

<para>
The source code for the packages distributed via Cygwin setup is also
available via Cygwin setup.
To install the source for the X server, run Cygwin setup and tick the
'Src?' check-box for the 'xorg-server' package.
</para>

<para>
This may have multiple patches applied on top of the upstream X.Org &x-window-system; source code,
and is known to build and function on Cygwin, so this should be the starting
point for new developers.
</para>

<para>
On installing the source code package, setup will unpack it under
/usr/src.  You should find the source archive,
any needed .patch files, and a .cygport file which automates the distribution
configuration, build and packaging tasks.
</para>

<note>
<para>
Due to the large number of patches applied to the upstream source, the current
source package contains a source archive prepared directly from a git repository
containing those patches on top of the upstream source, rather than containing
the upstream source archive and many separate patches.
</para>
</note>

<para>
The sources can unpacked and prepared using cygport as follows:
</para>

<screen>
&cygwin-bash-prompt; ~
$ cd /usr/src/xorg-server-n.nn.n-n.src

&cygwin-bash-prompt; /usr/src/xorg-server-n.nn.n-n.src
$ cygport xorg-server-n.nn.n-n.cygport prep
[lots of output as archive is unpacked and patches applied]

&cygwin-bash-prompt; /usr/src/xorg-server-n.nn.n-n.src
$ cygport xorg-server-n.nn.n-n.cygport compile
[lots of output as source is configured and built]

&cygwin-bash-prompt; /usr/src/xorg-server-n.nn.n-n.src
$ cd xorg-server-n.nn.n-n/src/xorg-server-n.nn.n/
[navigate to the source directory]

&cygwin-bash-prompt; /usr/src/xorg-server-n.nn.n-n/src/xorg-server-n.nn.n/
$
</screen>

<note>
<para>
Alternatively you may manually untar the archive and apply any patches
(in the correct order).
</para>
</note>

<note>
<para>
For details of using cygport to generate packages for distribution, see
<xref linkend="prog-distribution"></xref>
</para>
</note>

</sect2>

<sect2 id="prog-source-cygports">
<title>Obtaining from version control</title>

<para>
The packaging script for the packages distributed via Cygwin setup
is currently held in a git repository.
Intermediate versions between released packages can be obtained from there.
</para>

<screen>
&cygwin-bash-prompt; ~
$ git clone https://cygwin.com/git/cygwin-packages/xorg-server.git
</screen>

<para>
This will obtain a .cygport file. and any .patch files.  You can then add the source archive by downloading it with cygport.
</para>

<screen>
&cygwin-bash-prompt; ~
$ cygport xorg-server-n.n.n-n.cygport download
</screen>

<para>
Then proceed as in <xref linkend="prog-source-cygwin-setup"></xref>
</para>

</sect2>

<sect2 id="prog-source-xorg">
<title>Obtaining from X.Org</title>

<para>&project; source code is contained in, and distributed with, the
<ulink url="http://xorg.freedesktop.org/wiki/Releases/Download">
&x-window-system; source code releases</ulink>.
</para>

<para>Anonymous read-only access to the
<ulink url="http://cgit.freedesktop.org/xorg/xserver/">
&x-window-system; git source
</ulink>
tree hosted on
<ulink url="http://freedesktop.org/Software/xorg">freedesktop.org</ulink>
is available.</para>

<para>
<screen>
$ git clone git://anongit.freedesktop.org/git/xorg/xserver
</screen>
</para>

<para>
You will probably want to look at the .cygport file in the source
package obtained in <xref linkend="prog-source-cygwin-setup"/> and
check you understand if you need to use the configuration options
used there.
</para>

<para>
Consult the <glossterm linkend="gloss-git">git</glossterm> documentation
for details on using git.
</para>

<para>The CYGWIN branch exists in git for historical reasons.
Current development follows the mainline
(called the <emphasis>master</emphasis> branch in git terminology).
</para>

<para>If you just want to look at the Cygwin/X source, use the
<ulink url="http://cgit.freedesktop.org/xorg/xserver/">
cgit interface to the X.Org tree
</ulink>.
Most of the Cygwin/X-specific code is in the
<ulink url="http://cgit.freedesktop.org/xorg/xserver/tree/hw/xwin">
xserver/hw/xwin</ulink>
directory.
</para>

</sect2>

</sect1>
