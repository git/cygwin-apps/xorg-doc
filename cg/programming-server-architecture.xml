<sect1 id="prog-server-architecture">
<title>&title-prog-server-architecture;</title>

<para>&project;'s X Server architecture was heavily inspired by <xref
linkend="porting-layer-definition"></xref>, the Definition of the
Porting Layer for the X v11 Sample Server.</para>

<!-- BEGIN Server Privates -->

<sect2 id="prog-server-architecture-privates">
<title>&title-prog-server-architecture-privates;</title>

<para>X Servers use various structures to pass information around to
functions.  Some of those structures are colormaps, <glossterm
linkend="gloss-gc">graphics contexts</glossterm> (GCs), <glossterm
linkend="gloss-pixmap">pixmaps</glossterm>, and <glossterm
linkend="gloss-screen">screens</glossterm>.  The X protocol defines
the contents of each of these structures, however, the X Server
implementation and various X Server libraries (<glossterm
linkend="gloss-mi-layer">MI</glossterm>, <glossterm
linkend="gloss-fb-layer">FB</glossterm>, <glossterm
linkend="gloss-shadow-layer">Shadow</glossterm>, etc.)  may require
additional information to be associated with these internal
structures.  For example, the &project; X Server must associate a
&windows; window handle (hwnd) with each X Server screen that is
open.</para>

<para><glossterm linkend="gloss-privates">Privates</glossterm> are the
mechanism provided by the X protocol for associating additional
information with internal X Server structures.  Privates originally
consisted of a single pointer member contained in each structure,
usually named <structfield>devPrivate</structfield> or
<structfield>devPriv</structfield>.  This original specification only
allowed one of the X Server layers (mi, fb, shadow, etc.) to have
privates associated with an internal structure.  Privates have since
been revised.</para>

<para>The current privates implementation requires that each X Server
layer call a function on startup to indicate that that layer will
require privates and to obtain an index into the array of privates
that that layer's privates will be stored at.  Modern privates are
generally stored in an array of type <structname>DevUnion</structname>
pointed to by a structure member named
<structfield>devPrivates</structfield>;
<structname>DevUnion</structname> is defined in
<filename>xserver/include/miscstruct.h</filename>.  There
are two different memory allocation schemes for
<structfield>devPrivates</structfield>.</para>

<para>Memory for privates structures can either be preallocated or
allocated upon use.  Preallocation, the preferred method for GCs,
pixmaps, and windows, requires that the size of the privates memory
needed be specified during X Server initialization.  Preallocation
allows the <glossterm linkend="gloss-dix-layer">DIX</glossterm> layer
to allocate all memory needed for a given internal structure,
including all privates memory, as a single contiguous block of memory;
this greatly reduces memory fragmentation.  Allocation upon use, used
by screens, requires the <glossterm
linkend="gloss-ddx-layer">DDX</glossterm> structure creation function
to allocate memory for the privates;
<function>winScreenInit</function> calling
<function>winAllocatePrivates</function>, which allocates screen
privates memory directly, is an example of this.  Allocation upon use
can optionally and non-optimally be used by GCs, pixmaps, and
windows.</para>

<!-- BEGIN Structures -->
<!--
<sect3 id="prog-server-architecture-privates-structs">
<title>&title-prog-server-architecture-privates-structs;</title>

<para>Foo!</para>

</sect3>
-->
<!-- END Structures -->

<!-- BEGIN Macros -->

<sect3 id="prog-server-architecture-privates-macros">
<title>&title-prog-server-architecture-privates-macros;</title>

<para>Three macros are provided for each class of privates that make
setting up and using the privates easier.  The macros for screen
privates are examined as an example.</para>

<funcsynopsis>
<funcprototype>
  <funcdef>winPrivScreenPtr <function>winGetScreenPriv</function></funcdef>
  <paramdef>ScreenPtr <parameter>pScreen</parameter></paramdef>
</funcprototype>
</funcsynopsis>

<para><function>winGetScreenPriv</function> takes a non-NULL pointer
to a screen, a <type>ScreenPtr</type>, and returns the pointer stored
in the DDX privates for that screen.  Passing a NULL or invalid
<type>ScreenPtr</type> to <function>winGetScreenPriv</function> will
cause an access violation, crashing the &project; X Server.</para>

<funcsynopsis>
<funcprototype>
  <funcdef>void <function>winSetScreenPriv</function></funcdef>
  <paramdef>ScreenPtr <parameter>pScreen</parameter></paramdef>
  <paramdef>void * <parameter>pvPrivates</parameter></paramdef>
</funcprototype>
</funcsynopsis>

<para><function>winSetScreenPriv</function> takes a non-NULL pointer
to a screen, a <type>ScreenPtr</type>, and sets the DDX privates
pointer to the value of the <parameter>pvPrivates</parameter>
parameter.  Passing a NULL or invalid <type>ScreenPtr</type> to
<function>winSetScreenPriv</function> will cause an access violation,
crashing the &project; X Server.</para>

<funcsynopsis>
<funcprototype>
  <funcdef>void <function>winScreenPriv</function></funcdef>
  <paramdef>ScreenPtr <parameter>pScreen</parameter></paramdef>
</funcprototype>
</funcsynopsis>

<para><function>winScreenPriv</function> takes a non-NULL pointer to a
screen, a <type>ScreenPtr</type>, and declares a local variable in the
calling function named <varname>pScreenPriv</varname>.
<function>winScreenPriv</function> may only be called at the top of a
C function within the variable declaration block; calling the function
elsewhere will break the ANSI C rule that all variables must be
declared at the top of a scope block.  Passing a NULL or invalid
<type>ScreenPtr</type> to <function>winScreenPriv</function> will
cause an access violation, crashing the &project; X Server.</para>

</sect3>

<!-- END Macros -->

</sect2>

<!-- END Server Privates -->

<!-- BEGIN Engine System -->

<sect2 id="prog-server-architecture-engines">
<title>&title-prog-server-architecture-engines;</title>

<para>The &project; X Server uses several methods of drawing graphics
on the display device; each of these different drawing methods is
referred to as an engine.

It should be noted that the Primary FB engine is historical
and is discussed here only for completeness.</para>

<!-- BEGIN Shadow Drawing -->

<sect3 id="prog-server-architecture-engines-shadow">
<title>&title-prog-server-architecture-engines-shadow;</title>

<para>The Shadow FB engines use Keith Packard's <glossterm
linkend="gloss-fb-layer">FB</glossterm> drawing procedures wrapped
with his <glossterm linkend="gloss-shadow-layer">Shadow</glossterm>
layer that allows drawing to an <glossterm
linkend="gloss-offscreen-framebuffer">offscreen
framebuffer</glossterm> with periodic updates of the <glossterm
linkend="gloss-primary-framebuffer">primary
framebuffer</glossterm>.</para>

<para>
Currently, shadow FB engines exist using DirectDraw4 and GDI.
</para>

</sect3>

<!-- END Shadow Drawing -->

<!-- BEGIN Primary Drawing -->

<sect3 id="prog-server-architecture-engines-primary">
<title>&title-prog-server-architecture-engines-primary;</title>

<para>The Primary FB engine worked in the
same manner that the original &project; X Server worked, namely, it
uses <function>IDirectDrawSurface_Lock</function> to obtain a pointer
to the <glossterm linkend="gloss-primary-framebuffer">primary
framebuffer</glossterm> memory at server startup.  This memory pointer
is held until the X Server shuts down.  This technique does not work
on all versions of &windows;.</para>

<para>Locking the primary framebuffer on &windows; 95/98/Me causes the
Win16Mutex to be obtained by the program that locks the primary
framebuffer; the Win16Mutex is not released until the primary
framebuffer is unlocked.  The Win16Mutex is a semaphore introduced in
&windows; 95 that prevents 16 bit &windows; code from being reentered
by different threads or processes.  For compatibility reasons, all GDI
operations in &windows; 95/98/Me are written in 16 bit code, thus
requiring that the Win16Mutex be obtained before performing those
operations.  All of this leads to the following situation on &windows;
95/98/Me:</para>

<orderedlist numeration="arabic">
<listitem>
<para>The primary framebuffer is locked, causing the &project; X
Server to hold the Win16Mutex.</para>
</listitem>

<listitem>
<para>&windows; switches the &project; X Server out of the current
process slot; another process is switched in.</para>
</listitem>

<listitem>
<para>The newly selected process makes a GDI function call.</para>
</listitem>

<listitem>
<para>The GDI function call must wait for the Win16Mutex to be
released, but the Win16Mutex cannot be released until the &project; X
Server releases the Win16Mutex.  However, the &project; X Server will
not release the Win16Mutex until it exits.  The end result is that the
Win16Mutex has been deadlocked and the &windows; machine is frozen
with no way to recover.</para>
</listitem>

</orderedlist>

<para>&windows; NT/2000/XP do not contain any 16 bit code, so the
Win16Mutex is not an issue; thus, the Primary FB engine works fine on
those operating systems.  However, drawing directly to the primary
framebuffer suffers performance problems.  For example, on some
systems writing to the primary framebuffer requires doing memory reads
and writes across the PCI bus which is only 32 bits wide and features
a clock speed of 33 MHz, as opposed to accessing system memory, which
is attached to a 64 bit wide bus that runs at between 100 and 266
(effective) MHz.  Furthermore, accessing the primary framebuffer
memory requires several synchronization steps that take many clock
cycles to complete.  The end result is that the Primary FB engine is
several times slower than the Shadow FB engines.</para>

<para>The Primary FB engine also has several unique issues that are
difficult to program around.  Development of the Primary FB engine has
ceased, due to the difficulty of maintaining it, coupled with the fact
that Primary FB does not run on &windows; 95/98/Me and with the poor
performance of Primary FB.  The Primary FB source code has been left
in place so that future programmers can enable it and see the poor
performance of the engine for themselves.</para>

</sect3>

<!-- END Primary Drawing -->

</sect2>

<!-- END Engine System -->

<!-- BEGIN User Input -->

<sect2 id="prog-server-architecture-input">
<title>&title-prog-server-architecture-input;</title>

<para>
At the end of <function>InitInput</function> in <filename>hw/xwin/InitInput.c</filename>
we open <filename>/dev/windows</filename>, a special device which becomes ready when there
is anything to read on the windows message queue, and add that to the select mask for
<function>WaitForSomething</function> using <function>AddEnabledDevice</function>.
</para>

<para>
The X server's main loop calls the <glossterm linkend="gloss-os-layer">OS</glossterm> layer
<filename>os/WaitFor.c</filename>'s <function>WaitForSomething</function> function, which
waits for something to happen using <function>select</function>.
When <function>select</function> returns, all the wakeup handlers are run.
Any queued &win32; user input messages (as well as other &win32;
messages) are handled when <filename>hw/xwin/winwakeup.c</filename>'s
<function>winWakeupHandler</function> function is called.
Each &win32; user input message typically queues an input event, or several input
events, using the <glossterm linkend="gloss-mi-layer">MI</glossterm>
layer's <filename>mi/mieq.c</filename>'s <function>mieqEnqueue</function> function.
</para>

<para>
Enqueued MI input events are processed
when the <glossterm linkend="gloss-dix-layer">DIX</glossterm> layer
<filename>dix/dispatch.c</filename>'s <function>Dispatch</function>
function calls <filename>hw/xwin/InitInput.c</filename>'s
<function>ProcessInputEvents</function> function, which calls
<filename>mi/mieq.c</filename>'s <function>mieqProcessInputEvents</function>.
</para>

<!-- BEGIN Keyboard Input -->

<sect3 id="prog-server-architecture-input-keyboard">
<title>&title-prog-server-architecture-input-keyboard;</title>

<para>&win32; keyboard messages are processed in
<filename>winwndproc.c</filename>'s
<function>winWindowProc</function>.  The messages processed
are:</para>

<itemizedlist>

<listitem><para>WM_SYSKEYDOWN</para></listitem>

<listitem><para>WM_KEYDOWN</para></listitem>

<listitem><para>WM_SYSKEYUP</para></listitem>

<listitem><para>WM_KEYUP</para></listitem>

</itemizedlist>

<para>The WM_SYSKEY* messages are generated when the user presses a
key while holding down the <keycap>Alt</keycap> key or when the user
presses a key after pressing and releasing the <keycap>F10</keycap>
key.  Processing for WM_SYSKEYDOWN and WM_KEYDOWN (and respectively
WM_SYSKEYUP and WM_KEYUP) messages are identical because the X Server
does not distinguish between a normal key press and a key press when
the <keycap>Alt</keycap> key is down.</para>

<para>&win32; uses virtual key codes to identify which key is being
pressed or released.  Virtual key codes follow the idea that the same
virtual key code will be sent for keys with the same label printed on
them.  For example, the left and right <keycap>Ctrl</keycap> keys both
generate the VK_CONTROL virtual key code.  Virtual key codes are
accompanied by other state information, such as the extended flag,
that distinguishes between the multiple keys with the same label.  For
example, the left <keycap>Ctrl</keycap> key does not have the extended
flag asserted, while the right <keycap>Ctrl</keycap> key does have the
extended flag asserted.  However, virtual key codes are not the way
that key presses have traditionally been identified on personal
computers and in the X Protocol.</para>

<para>Personal computers and the X Protocol use scan codes to identify
which key is being pressed.  Each key on the keyboard generates a
specified number when that key is pressed or released; this number is
called the scan code.  Scan codes are always distinct for distinct
keys.  For example, the left and right <keycap>Ctrl</keycap> keys
generate distinct scan codes, even though their functionality is the
same.  Scan codes do not have additional state information, as the
multiple keys with the same label will each generate a unique scan
code.  There is some debate as to which of virtual key codes or scan
codes is the better system.</para>

<para>The X Protocol expects that keyboard input will be based on a
scan code system.  There are two methods of sending a scan codes from
a virtual key code message.  The first method is to create a static
table that links the normal and extended state of each virtual key
code to a scan code.  This method seems valid, but the method does not
work reliably for users with non-U.S. keyboard layouts.  The second
method simply pulls the scan code out of the
<parameter>lParam</parameter> of the keyboard messages; this method
works reliably for non-U.S. keyboard layouts.  However, there are
further concerns for non-U.S. keyboard layouts.</para>

<para>Non-U.S. keyboard layouts typically use the right
<keycap>Alt</keycap> key as an alternate shift key to access an
additional row of symbols from the <keycap>`</keycap>,
<keycap>1</keycap>, <keycap>2</keycap>, ..., <keycap>0</keycap> keys,
as well as accented forms of standard alphabetic characters, such as
&aacute;, &auml;, &aring;, &uacute; and additional alphabetic characters, such as &szlig;.
Non-U.S. keyboards typically label the right <keycap>Alt</keycap> key
as <keycap>AltGr</keycap> or <keycap>AltLang</keycap>; the
<abbrev>Gr</abbrev> is short for <quote>grave</quote>, which is the
name of one of the accent symbols.  The X Protocol and &win32; methods
of handling the <keycap>AltGr</keycap> key are not directly compatible
with one another.</para>

<para>The X Protocol handles <keycap>AltGr</keycap> presses and
releases in much the same way as any other key press and release.
&win32;, however, generates a fake <keycap>Ctrl</keycap> press and
release for each <keycap>AltGr</keycap> press and release.  The X
Protocol does not expect this fake <keycap>Ctrl</keycap> press and
release, so care must be taken to discard the fake
<keycap>Ctrl</keycap> press and release.  Fake <keycap>Ctrl</keycap>
presses and releases are detected and discarded by passing each
keyboard message to <filename>winkeybd.c</filename>'s
<function>winIsFakeCtrl_L</function> function.
<function>winIsFakeCtrl_L</function> detects the fake key presses and
releases by comparing the timestamps of the <keycap>AltGr</keycap>
message with the timestamp of any preceding or trailing
<keycap>Ctrl</keycap> message.  Two real key events will never have
the same timestamp, but the fake key events have the same timestamp as
the <keycap>AltGr</keycap> messages, so the fake messages can be easily
identified.</para>

<para>Special keyboard considerations must be handled when the &project; X
Server loses or gains the keyboard focus.  For example, the user can
switch out of &project;, toggle the <keycap>Num Lock</keycap> key,
then switch back into &project;; in this case &project; would not have
received the <keycap>Num Lock</keycap> toggle message, so it will
continue to function as if <keycap>Num Lock</keycap> was in its
previous state.  Thus, the state of any mode keys such as <keycap>Num
Lock</keycap>, <keycap>Caps Lock</keycap>, <keycap>Scroll
Lock</keycap>, and <keycap>Kana Lock</keycap> must be stored upon loss
of keyboard focus; on regaining focus, the stored state of each mode key must then be
compared to that key's current state, toggling the key if its state
has changed.</para>

</sect3>

<!-- END Keyboard Input -->

<!-- BEGIN Mouse Input -->

<sect3 id="prog-server-architecture-input-mouse">
<title>&title-prog-server-architecture-input-mouse;</title>

<para>&win32; mouse messages are processed in
<filename>winwndproc.c</filename>'s
<function>winWindowProc</function>.  The messages processed
are:</para>

<itemizedlist>

<listitem><para>WM_MOUSEMOVE</para></listitem>

<listitem><para>WM_NCMOUSEMOVE</para></listitem>

<listitem><para>WM_LBUTTON*</para></listitem>

<listitem><para>WM_MBUTTON*</para></listitem>

<listitem><para>WM_RBUTTON*</para></listitem>

<listitem><para>WM_MOUSEWHEEL</para></listitem>

</itemizedlist>

<para>Handling mouse motion is relatively straight forward, with the
special consideration that the &windows; mouse cursor must be hidden
when the mouse is moving over the client area of a &project; window;
the &windows; mouse cursor must be redisplayed when the mouse is
moving over the non-client area of a &project; window.  &win32; sends
the absolute coordinates of the mouse, so we call
<function>miPointerAbsoluteCursor</function> to change the position of
the mouse.</para>

<para>Three-button mouse emulation is supported for users that do not
have a three button mouse.  When three-button mouse emulation is
disabled, mouse button presses and releases are handled trivially in
<filename>winmouse.c</filename>'s
<function>winMouseButtonsHandle</function> by simply passing the event
to <function>mieqEnqueue</function>.  Three-button mouse emulation is
quite complicated.</para>

<para>Three-button mouse emulation is handled by starting a timer when
the left or right mouse buttons are pressed; the button event is sent
as a left or right mouse button event if the other button is not
pressed before the timer expires.  The button event is sent as an
emulated middle button event if the other mouse button is pressed
before the timer runs out.</para>

<para>The mouse wheel is handled in <filename>winmouse.c</filename>'s
<function>winMouseWheel</function> by generating sequences of button 4
and button 5 presses and releases corresponding to how much the mouse
wheel has moved.  &win32; uses variable resolution for the mouse wheel
and passes the mouse wheel motion as a delta from the wheel's previous
position.  The number of button clicks to send is determined by
dividing the wheel delta by the distance that is considered by &win32;
to be one unit of motion for the mouse wheel; any remainder of the
wheel delta must be preserved and added to the next mouse wheel
message.</para>

</sect3>

<!-- END Mouse Input -->

<!-- BEGIN Mouse Input -->

<sect3 id="prog-server-architecture-other-windows-messages">
<title>Other Windows messages</title>

<para>
Certain other WM_ messages are also processed. TBD.
</para>

</sect3>

</sect2>

<!-- END User Input -->

</sect1>
