<qandadiv id="i18n">
<title>Internationalization</title>

<qandadiv id="i18n-keyboard">
<title>Keyboard support</title>

<qandaentry>
<question id="q-non-U.S.-keyboard-layout">
<para>How do I use a non-U.S. keyboard layout?</para>
</question>
<answer>

<para>Some keyboard layouts are autodetected from the Windows
keyboard settings.  For these layouts no special change is needed.
For all other layouts there is the possibility to configure the layout
via commandline options.</para>

<para>The main option for changing the layout is <parameter>-xkblayout
<replaceable>countrycode</replaceable></parameter> where
<replaceable>countrycode</replaceable> is in most cases the 2
character code which also represents the country in internet adresses
(e.g.  Australia = au, Deutschland = de, France = fr, Japan = jp)</para>

<para>Other options for tweaking the XKB layout are
<parameter>-xkbmodel</parameter>,<parameter>-xkbvariant</parameter>,
<parameter>-xkboptions</parameter> and
<parameter>-xkbrules</parameter>.  These are the counterparts for the
similar named options known from the xorg.conf file.</para>

<para>If the loading fails, check <xref
linkend="q-xkb-not-working"></xref></para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-submit-layout">
<para>Is there a way to add a layout to the list of autodetected layouts?</para>
</question>
<answer>
<para>If your keyboard layout is not automatically detected you can send 
the required information for including it into &project; to &project-ml;. 
Please include the following information in your mail:</para>

<itemizedlist>
<listitem>
<para>The windows keyboard layout code and the layout name</para>
<para>You will find it in <filename>&file-log;</filename> in lines similar
to these:
<screen>(--) winConfigKeyboard - Layout: "00001809" (00001809)
(EE) Keyboardlayout "Irish" (00001809) is unknown
</screen>
</para>
</listitem>
<listitem>
<para>The XKB layout code for this layout if you know it.  Please experiment 
with <command>setxkbmap</command> or  <parameter>-xkblayout</parameter> (as
described in <xref linkend="q-non-U.S.-keyboard-layout"/>) to find an XKB layout
code which works for you, otherwise the  maintainers will have to guess it.</para>
</listitem>
<listitem>
<para>A description how the layout looks like. This makes it easy to identify
the matching XKB layout code. Many layouts are available from the 
<ulink url="http://www.microsoft.com/globaldev/reference/keyboards.mspx">
Microsoft Global Dev</ulink> website
<emphasis>(It seems that website only works with some browsers)</emphasis>.
Just add a link to your layout.</para>
</listitem>
</itemizedlist>
</answer>
</qandaentry>


<qandaentry>
<question id="q-modmap-obtaining">
<para>Where can I find an xmodmap for my non-U.S. keyboard layout?</para>
</question>

<answer>
<para>This question should be obsolete
The package <filename>xkeyboard-config</filename> should contain just about
any needed layout</para>
</answer>
<answer>
<para>Or, you can use <command>xkeycaps</command> to automatically
generate a modmap for one of over 208 different layouts.  See the
<ulink url="&xkeycaps-home;">xkeycaps home page</ulink> to download
and for more information.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-how-modmap-and-xdmcp">
<para>How do I get my non-U.S. keyboard modmap to be installed when
using xdmcp?</para>
</question>

<answer>
<para>See <xref linkend="q-non-U.S.-keyboard-layout"></xref></para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-aix-xkb">
<para>Logging into AIX via XDMCP causes the keyboard to function as if
<keycap>AltGr</keycap> is permanently pressed.</para>
</question>

<answer>
<para>
[paraphrased from the <ulink url="http://www.straightrunning.com/XmingNotes/trouble.php">Xming FAQ</ulink>]
AIX login scripts contain a call to xmodmap (for IBM keyboards) which causes
the keyboard to be incorrectly configured for XWin.
Commenting out those calls should allow you to use XWin with AIX.
</para>

<para>
The XKB extension is now always enabled in the X.Org xserver, so the previous
answer to this question of disabling the XKB extension with the <parameter>-kb</parameter>
parameter is no longer applicable.
</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-xkb-not-working">
<para>Loading an XKB keyboard layout selected with -xkblayout fails</para>
</question>

<answer>
<para>Not all keyboard layouts are tested very well and some contain
errors or do not work at all. To test if the compiling of your layout
works start

<screen>
setxkbmap <replaceable>de</replaceable> -print | xkbcomp -w3 -xkm - :0.0
</screen>

(replace the "de" with your layout code).  This may produce warnings,
but must not produce errors. If there are errors then please report
them to the mailing list.</para>

</answer>
</qandaentry>


<qandaentry>
<question id="alt-gr-win-xp-powertoys">
<para>I have Windows XP with Powertoys installed and AltGr does not work. 
What can I do?</para>
</question>

<answer>
<para>This question should be obsolete.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="alt-gr-with-old-x">
<para>AltGr does not work properly when connecting to various older commercial unices
(e.g. HP-UX, AIX) or to old XFree86.</para>
</question>

<answer>

<para>
<filename>xkeyboard-config</filename> XKB keyboard layouts generally have AltGr
mapped as ISO_Level3_Shift to access additional characters on non-english keyboards.

For reasons unknown to us, this is incompatible with some older X11 releases.
We don't have access to such a machine, so we are unable to track this down and
find a reason.
</para>

<para>
It has been reported that sometimes it helps to run

<screen>
DISPLAY=:0.0 setxkbmap <replaceable>languagecode</replaceable>
</screen>

from a cygwin shell after connecting.</para>

<para>
See this <ulink url="http://cygwin.com/ml/cygwin-xfree/2011-07/msg00011.html">
mailing list thread</ulink> for more discussion and a possible workaround.
</para>

</answer>
</qandaentry>



</qandadiv>

<qandadiv id="i18n-display">
<title>Display problems</title>

<qandaentry>
<question id="q-bash-extended-chars">
<para>How do I get <command>bash</command> to display accents and/or
umlauts?</para>
</question>

<answer>
<para>(Heinz Peter Hippenstiel) Add the following lines to
<filename>.inputrc</filename> in your &cygwin; home directory
(e.g. <filename>~/.inputrc</filename>):
</para>

<screen>
set input-meta on    # to accept 8-bit characters
set output-meta on   # to show 8-bit characters
set convert-meta off # to show it as character, not the octal representation
</screen>
</answer>
</qandaentry>

<qandaentry>
<question id="q-bash-8bit">
<para>How do I put <command>bash</command> into <quote>8 bit</quote>
mode?</para>
</question>

<answer>
<para>See <xref linkend="q-bash-extended-chars"></xref></para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-xterm-utf8">
<para>How do I display unicode characters in an <command>xterm</command>?
</para>
</question>

<answer>
<label>A1: for Cygwin 1.7</label>
<para>
If you have a UTF-8 locale configured, this should all just work :-).
</para>

<para>
To confirm this is working properly, you may try the following:
</para>

<screen>
$ wget http://www.cl.cam.ac.uk/~mgk25/ucs/examples/quickbrown.txt
[...]
$ cat quickbrown.txt
</screen>

<screen>
$ wget http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-demo.txt
[...]
$ cat UTF-8-demo.txt
</screen>

<para>
If you want to be able type unicode characters into this <command>xterm</command>, you'll need to configure
your <command>bash</command> shell not to escape 8-bit characters, see <xref linkend="q-bash-extended-chars"></xref>
</para>

</answer>

<answer>
<label>A2: for Cygwin 1.5</label>
<para>
Start your <command>xterm</command> in UTF-8 mode as <command>xterm +lc -u8</command>.
</para>

<para>
To confirm this is working properly, you may try the following
</para>

<screen>
$ wget http://www.cl.cam.ac.uk/~mgk25/ucs/examples/quickbrown.txt
[...]
$ cat quickbrown.txt
</screen>

<para>
For reasons I don't currently understand, the default fixed font is only capable
of supplying accented roman, hiragana and katakana characters, so if you
wish to work with e.g. greek, cyrillic, hebrew, thai, etc. you'll need to
start your xterm specifying a suitable font e.g.
<command>xterm +lc -u8 -fn -misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso10646-1</command>
</para>

<para>
To confirm this is working properly, you may try the following
</para>

<screen>
$ wget http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-demo.txt
[...]
$ cat UTF-8-demo.txt
</screen>

<para>
For other programs run from your xterm to output properly (e.g. <command>less</command>, which is why <command>cat
</command> is used in the examples above), you may also need to set the LANG environment variable to
<replaceable>LL_CC</replaceable>.UTF-8, where LL_CC is your language and country code.
</para>

<para>
If you want to be able type unicode characters into this <command>xterm</command>, you'll need to configure
your <command>bash</command> shell not to escape 8-bit characters, see <xref linkend="q-bash-extended-chars"></xref>
</para>

<para>
See also <ulink url="http://cygwin.com/faq/faq.html#faq.using.unicode">the main Cygwin FAQ question on unicode
support in Cygwin</ulink>
</para>
</answer>
</qandaentry>

</qandadiv>
</qandadiv>
