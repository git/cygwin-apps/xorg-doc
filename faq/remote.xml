<qandadiv id="remote">
<title>Remote connections</title>

<qandaentry>
<question id="q-ssh-no-x11forwarding">
<para>X11Forwarding does not work with OpenSSH under &cygwin;</para>
</question>

<answer>
<label>A1:</label>
<para>
Try adding the <parameter>-v</parameter> option to <command>ssh</command>, which often pinpoints the reason for a connection problem.
</para>

<para>
From the <command>ssh</command> man-page :
<emphasis>-v Verbose mode.  Causes ssh to print debugging messages about its progress.
This is helpful in debugging connection, authentication, and configuration problems.
Multiple -v options increase the verbosity.  The maximum is 3.</emphasis>
</para>
</answer>

<answer>
<label>A2:</label>

<para>Before establishing the ssh connection the xserver must be
started and the environment variable &DISPLAY;
must be set.

<screen>
$ DISPLAY=<replaceable>:0.0</replaceable>
$ export DISPLAY
$ ssh -Y <replaceable>remotehost</replaceable>
</screen>

or

<screen>
$ DISPLAY=<replaceable>:0.0</replaceable> ssh -Y <replaceable>remotehost</replaceable>
</screen>
</para>

</answer>

<answer>
<label>A3:</label>
<para>
Make sure you're not starting <command>ssh</command> with the option
<parameter>-X</parameter>.  Since OpenSSH 7.2p1, <command>ssh</command> does
<emphasis>not</emphasis> fallback to trusted forwarding, option <parameter>
-Y</parameter>, so no X11 forwarding is setup.  Use <command>ssh -Y</command>.
</para>

<para>
Make sure you're not starting <command>ssh</command> with the option <parameter>-x</parameter>
(lowercase). This disables X11 forwarding.
</para>
</answer>

<answer>
<label>A4:</label>

<para>Check that X11Forwarding is not disabled in the ssh client
configuration.</para>

<para>The configfiles are by default
<filename>~/.ssh/config</filename> and
<filename>/etc/ssh_config</filename>.  The file in the home directory
overrides settings in the global one.</para>

<para>The configfile is split into various sections starting with
<quote>Host <replaceable>wildcard</replaceable></quote>.  The section
applies to all hosts where <replaceable>wildcard</replaceable> matches
the hostname.</para>

<para>If this section contains an entry <quote>ForwardX11 no</quote>
then X11Forwarding is disabled. To enable it change the entry to:
</para>

<screen>
ForwardX11 yes
</screen>
</answer>

<answer>
<label>A5:</label>

<para>Check that X11Forwarding is not disabled in the ssh server
configuration.</para>

<para>The configfile is by default
<filename>/etc/ssh/sshd_config</filename>.  If there is an entry
<quote>X11Forwarding no</quote> then X11Forwarding is
disabled. </para>

<para>If you have write access to the config file then change it to

<screen>
X11Forwarding yes
</screen>

The OpenSSH server must be restarted or SIGHUP'ed to re-read the configuration
file after it is changed.

Otherwise, ask your administrator to change this for you.</para>

</answer>

<answer>
<label>A6:</label>

<para>
[Frederick W. Wheeler]
If the <emphasis>remote</emphasis> machine is a Windows machine using &cygwin; OpenSSH server,
make sure the &cygwin; <command>xauth</command> package is installed on the <emphasis>remote</emphasis>
machine.  The OpenSSH server needs to be able to run <command>xauth</command> to do X11 Forwarding.
</para>

</answer>

</qandaentry>

<qandaentry>
<question id="q-bad-atom">
<para>Why do remote programs crash with an <errorname>X Error of failed request: BadAtom</errorname>?
Why do remote programs exit when you try to copy and paste?
</para>
</question>

<answer>
<para>This question should be obsolete since the SECURITY extension
is now disabled</para>

<para>OpenSSH 3.8 enables untrusted &X11Forwarding; by default when
connecting to an ssh server that supports it.</para>

<para> You will
quickly notice that this is the case if most of your X applications
are now killed when you try to copy and paste, X applications fail
with an error similar to that below,  or if
<command>xdpyinfo</command> returns only a fraction of the supported
extensions that it does if run locally.</para>

<screen> X Error of failed request: BadAtom (invalid Atom parameter)
  Major opcode of failed request: 18 (X_ChangeProperty)
  Atom id in failed request: 0x114
  Serial number of failed request: 370
  Current serial number in output stream: 372
</screen>

<para>It is easiest to just override untrusted X11Forwarding by passing
<parameter>-Y</parameter> to <command>ssh</command> in place of
<parameter>-X</parameter>.  The <parameter>-Y</parameter> does the
same thing as <parameter>-X</parameter>, but it enables trusted X11 forwarding
for the current connection.</para>

<para>Setting
<quote>ForwardX11Trusted yes</quote> in the ssh client configuration file
does the same thing. See <command>man ssh_config</command> for more information.
</para>

<para>
See also <xref linkend="q-ssh-no-x11forwarding"></xref>
</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-trusted-untrusted-x11-forwarding">
<para>I'm confused about the difference between trusted and untrusted X11 forwarding.
What does "Warning: untrusted X11 forwarding setup failed: xauth key data not generated" mean?
Why is the SECURITY extension disabled?
</para>
</question>

<answer>
<para>
The warning means that ssh is going to use <emphasis>trusted</emphasis> X11 forwarding
because <emphasis>untrusted</emphasis> X11 forwarding depends on the SECURITY extension,
which isn't built into the X server and has been disabled by default upstream.
</para>

<para>
Trusted X11 forwarding means that you trust the server that you wish to
ssh into.
The X server will allow remote clients to do whatever a local client would
be able to do to your X session, for example, monitor your keypresses and
take a screenshot.
Such programs could be run by a malicious or compromised root user on
the ssh server, or under your account if it was compromised on the ssh server.
</para>

<para>
Starting with OpenSSH 3.8, untrusted forwarding is the default when X forwarding
is requested using the <parameter>-X</parameter> command line option and you need
to use the option <parameter>-Y</parameter> or specify
<quote>ForwardX11Trusted yes</quote> in the client configuration for trusted forwarding
by default.
</para>

<para>
Since OpenSSH 7.2p1, untrusted forwarding (<command>ssh</command>
<parameter>-X</parameter>) does <emphasis>not</emphasis> fallback to
<command>ssh</command> <parameter>-Y</parameter> trusted forwarding.</para>

<para>
So why is this disabled?
Untrusted X11 forwarding was meant to be a way to allow logins to
unknown or insecure systems.  It generates a cookie with xauth and uses
the security extension to limit what the remote client is allowed to do.
But this is widely considered to be not useful, because the security
extension uses an arbitrary and limited access control policy, which
results in a lot of applications not working correctly (e.g. not being
able to cut and paste) and what is really a false sense of security.
See this mail <ulink url="http://cygwin.com/ml/cygwin-xfree/2008-11/msg00154.html">for more on the subject</ulink>.
</para>

<para>
(Words adapted from an email by Yaakov Selkowitz)
</para>

</answer>
</qandaentry>

<qandaentry>
<question id="q-warning-no-xauth-data">
<para>
What does "Warning: no xauth data; using fake authentication data for X11 forwarding" mean?
</para>
</question>

<answer>
<para>
Unless you started the X server with the <parameter>-auth</parameter> option
(typically by using <command>startx</command>) this warning is expected and
can safely be ignored.
</para>
</answer>

</qandaentry>

<qandaentry>
<question id="q-twenty-minute-timeout">
<para>
Why can't new remote X clients connect to the X server after 20 minutes?
</para>
</question>

<answer>
<para>
Starting with OpenSSH 5.6, ssh enforces the ForwardX11Timeout (which defaults
to 1200 seconds) when an untrusted connections is requested, even if an untrusted
connection could not be made (e.g. you used <command>ssh -X</command> which asks for
an untrusted connection, and got the "untrusted X11 forwarding setup failed" warning).
This means that no new connections to the X server
can be made 20 minutes after the ssh connection is established.
</para>

<para>
Use <command>ssh -Y</command>.  See also <xref linkend="q-bad-atom"></xref>.
</para>
</answer>

</qandaentry>

<qandaentry>
<question id="q-remote-clients-cant-connect">
<para>
Remote clients can't connect
</para>
</question>

<answer>
<para>
The X server now uses <parameter>-nolisten tcp</parameter> by default, which increases
the security of the X server by not opening a TCP/IP socket.
</para>

<para>
Use the <parameter>-listen tcp</parameter> option to allow the X server to open
a TCP/IP socket as well, e.g. <command>startxwin -- -listen tcp</command>.
See <xref linkend="q-command-line-args"/>.
</para>

<para>
A better solution is to stop explicitly setting DISPLAY and allowing access using
<command>xhost</command> or by disabling access control.
Use <command>ssh -Y</command> instead.  (See the User's Guide section <ulink
url="http://x.cygwin.com/docs/ug/using-remote-apps.html#using-remote-apps-ssh">on
X forwarding using ssh</ulink> for more details).
</para>
</answer>

</qandaentry>

<qandaentry>
<question id="q-local-noncygwin-clients-cant-connect">
<para>
X sessions forwarded by <command>PuTTY</command> can't connect.
Non-cygwin local X clients can't connect.
</para>
</question>

<answer>
<para>
The X server now uses <parameter>-nolisten tcp</parameter> by default, which increases
the security of the X server by not opening a TCP/IP socket, only a local (UNIX
domain) socket.  Non-cygwin applications cannot connect to that socket.
</para>

<para>
Use the <parameter>-listen tcp</parameter> option to allow the X server to open
a TCP/IP socket as well, e.g. <command>startxwin -- -listen tcp</command>.
See <xref linkend="q-command-line-args"/>.
</para>
</answer>

</qandaentry>

</qandadiv>

<qandadiv id="xdmcp">
<title>XDMCP connections</title>

<qandaentry>
<question id="q-xdmcp-no-valid-address">
<para><errorname>XDMCP fatal error: Session declined No valid
address</errorname></para>
</question>

<answer>
<para>&project; is sometimes unable to determine which local network
interface's address should be reported to the &XDMCP; server; in these 
cases you need to pass <parameter>-from</parameter> <replaceable
class="option">local_host_name_or_ip_address</replaceable> to
&XWin; to specify which interface address to report.</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-audit-client-rejected">
<para>Why does &project; report AUDIT: client 1 rejected from IP 
<replaceable>remotehost</replaceable>?</para>
</question>
<answer>
<para>The problem is most likely a wrong DNS (Network name resolution). Make 
sure your windows host has a hostname which is valid from linux too and an IP 
address which linux can resolve to that hostname.</para>
<para>If you add a line
<screen>
192.168.26.1 myhost
</screen>
to /etc/hosts on the &XDMCP; server with the IP address and the hostname 
of your windows host the name resolution should work.
</para>
</answer>

</qandaentry>

<qandaentry>
<question id="q-xdmcp-query">
<para>I get no login screen when using <parameter>-query</parameter></para>
</question>

<answer>
<label>A1: Disabled XDMCP on servers</label>
<para>[Mika Laitio] For security reasons, &XDMCP; is not enabled by
default on most Linux/UNIX/*NIX/*BSD distributions (Red Hat, Mandrake,
SuSE, FreeBSD, NetBSD, etc.) by default.  You have to manually enable
remote logins to your X Display Manager (e.g. <command>xdm</command>,
<command>kdm</command>, or <command>gdm</command>).  The location of
the proper config file is distribution/OS dependent, but a short list of
known config file locations is given in <xref
linkend="tbl-config-files"/>.  You must change the line:
<screen>
[Xdmcp]
Enable=false
</screen>
to:
<screen>
[Xdmcp]
Enable=true
</screen>
or for xdm style configuration:
<screen>
DisplayManager.requestPort:     0
</screen>
to:
<screen>
!DisplayManager.requestPort:     0
</screen>
</para>

<table id="tbl-config-files" frame="all">
<title>Known XDM Configuration File Locations</title>

<tgroup cols="4">

<thead>
<row>
<entry>Distribution/OS</entry>
<entry>Version</entry>
<entry>Display Manager</entry>
<entry>Location</entry>
</row>
</thead>

<tbody valign="top">

<row>
<entry>Linux Mandrake</entry>
<entry>8.1</entry>
<entry>kdm</entry>
<entry>/usr/share/config/kdm/kdmrc</entry>
</row>

<row>
<entry>Debian GNU/Linux</entry>
<entry>Unstable</entry>
<entry>kdm</entry>
<entry>/etc/kde3/kdm/kdmrc</entry>
</row>

<row>
<entry>Debian GNU/Linux</entry>
<entry>Unstable</entry>
<entry>gdm</entry>
<entry>/etc/X11/gdm/gdm.conf</entry>
</row>

<row>
<entry>Debian GNU/Linux</entry>
<entry>Unstable</entry>
<entry>xdm</entry>
<entry>/etc/X11/xdm/xdm-config</entry>
</row>

<row>
<entry>Debian GNU/Linux</entry>
<entry>Unstable</entry>
<entry>wdm</entry>
<entry>/etc/X11/wdm/wdm-config</entry>
</row>

</tbody>
</tgroup>
</table>
</answer>

<answer>
<label>A2: XDMCP and firewalls</label>
<para>&XDMCP; will not work correctly if you have a personal firewall installed
or the built-in firewall of Windows is activated.</para>
<para>The XDMCP protocol will send and receive data on port 177/UDP. But the 
actual connections will be made to the local port 6000/TCP. It is safe to 
allow connections since the xserver has an own security layer. An overview 
of used ports is given in <xref linkend="tbl-xdmcp-ports"/>.
</para>

<table id="tbl-xdmcp-ports" frame="all">
<title>Ports used with XDMCP connections</title>

<tgroup cols="4">

<thead>
<row>
<entry>Port</entry>
<entry>Protocol</entry>
<entry>Direction</entry>
<entry>Comment</entry>
</row>
</thead>

<tbody valign="top">
<row>
<entry>177</entry>
<entry>UDP</entry>
<entry>Incoming/Outgoing</entry>
<entry>Actual XDMCP connection</entry>
</row>

<row>
<entry>6000+<parameter>display</parameter></entry>
<entry>TCP</entry>
<entry>Incoming</entry>
<entry>Connection for X11 clients. <parameter>display</parameter> is 
usually 0 except if you specify it on the commandline.</entry>
</row>
</tbody>
</tgroup>
</table>

</answer>
</qandaentry>

<qandaentry>
<question id="q-mandrake-8.1-xdmcp">
<para>XDMCP does not work with Mandrake 8.1</para>
</question>
<answer><para>See <xref linkend="q-xdmcp-query"></xref></para></answer>
</qandaentry>

<qandaentry>
<question id="q-xdmcp-gdm">
<para>Why does GDM not work with <parameter>-clipboard</parameter></para>
</question>
<answer>
<para>
Newer versions of GDM have a more complex startup mechanism than the other
display manager have. This can interfere with the way the clipboard integration
client is started.
</para>
<para>
Workaround: add (or modify) this section in the <filename>gdm.conf</filename> 
(or <filename>/etc/gdm/custom.conf</filename>) file.
<screen>
[daemon]
KillInitClients=false
</screen>
</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-solaris-fonts">
<para>I get no login screen for Solaris</para>
</question>

<answer>
<para>See also <xref linkend="q-remote-solaris"></xref> and 
<xref linkend="q-cde-via-xdmcp-hangs"></xref></para>
</answer>

<answer>
<para>[David Dawson] For whatever reason, certain versions of Solaris
need fonts that are not provided by &project;; the result is that you
may see the Solaris background tile and the hourglass cursor, but the
XDM login prompt will never appear.  The simplest solution is to point
&project; at the font server that is usually running on the Solaris
machine.  You'll need a command line similar to the following to start
your &XDMCP; session and to connect to the Solaris font server:</para>

<para>
<userinput>&file-server-exe; -query
<replaceable>solaris_hostname_or_ip_address</replaceable> -fp
tcp/<replaceable>solaris_hostname_or_ip_address</replaceable>:7100</userinput>
</para>

<note>
<para>The <parameter>-fp</parameter> parameter is a general X Server
parameter, it is not specific to &project;; therefore, the
<parameter>-fp</parameter> is documented in the <ulink
url="&xorg-url-man-xserver;">X Server manual page</ulink>.  For
additional information about fonts, see <ulink
url="&xorg-url-fonts;">&xorg-title-fonts;</ulink>.</para>
</note>

<para>The standard port number for a font server is
<literal>7100</literal>, however, you may need to ask your system
administrator what the font server port number is if you cannot
connect to a font server on port <literal>7100</literal>.  It is also
possible that your Solaris machine is not running a font server, in
which case you will need to consult your Solaris documentation for
instructions on how to run a font server.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-remote-solaris">
<para>XDMCP freezes with remote Solaris machine!</para>
</question>

<answer>
<para>See also <xref linkend="q-solaris-fonts"></xref> and 
<xref linkend="q-cde-via-xdmcp-hangs"></xref></para>
</answer>

<answer>
<para>Solaris appears to not support certain display bit depths, such
as 24 bits per pixel.  Change your &windows; display bit depth to 8,
16, or 32 and try logging in again.  File a complaint with Sun if this
issue is important to you, or change your Solaris machines to use
&xfree; instead of the Solaris X Window System.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-cde-via-xdmcp-hangs">
<para>Login to <acronym>CDE</acronym> on &sun-solaris; via XDMCP hangs
&project;.</para>
</question>

<answer>
<para>See also <xref linkend="q-solaris-fonts"></xref> and <xref
linkend="q-remote-solaris"></xref></para>
</answer>

<answer>
<para>Install the <ulink url="&sun-solaris-url-patches;">recommended
set of patches</ulink> for your version of &sun-solaris;.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-xdmcp-info">
<para>Where can I find more information about XDMCP.</para>
</question>

<answer>
<para>See the <ulink
url="&linuxdoc-url-xdmcp;">&linuxdoc-title-xdmcp;</ulink> for more
information about XDMCP.</para>
</answer>
</qandaentry>

</qandadiv>
