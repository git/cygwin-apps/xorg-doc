<qandadiv id="errors">
<title>&title-errors;</title>

<qandaentry>
<question id="q-owner-tmp-.X11-unix">
<para><errorname>_XSERVTransmkdir: Owner of /tmp/.X11-unix should be set to root</errorname></para>
</question>

<answer>
<para>
<screen>
_XSERVTransmkdir: Owner of /tmp/.X11-unix should be set to root
_XSERVTransmkdir: ERROR: euid != 0,directory /tmp/.X11-unix will not be created
</screen>
</para>

<para>This question should be obsolete as this error is no longer generated.</para>
<para>This warning message can be ignored; it does not cause any known
problems.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-error-security-policy">
<para><errorname>error opening security policy file
/usr/X11R6/lib/X11/xserver/SecurityPolicy</errorname></para>
</question>

<answer>
<para>This question should be obsolete as this error is no longer generated.</para>
<para>This error is harmless.</para>
</answer>
</qandaentry>

<qandaentry>
<question id="duplicate-invocation">
<para>Duplicate invocation on display number: 0.  Exiting.</para>
</question>
<answer>
<para>Most likely you have started &XWin; twice.</para>

<para>if you start multiple instances of &XWin; you have to
give then unique display numbers
</para>

<para><screen>&file-server-exe; -query foo
&file-server-exe; :1 -query bar
&file-server-exe; :2 -query blubb
</screen>
Specifying no display number is the same as using <parameter>:0</parameter></para>

<para>If you want another terminal window (which in fact is just a convenient side effect of running <filename>startxwin</filename>) you should do this by starting <filename>xterm &</filename> from an existing terminal window, from the notification area icon menu, from a cygwin shell or from the start menu.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-error-font-eof">
<para><errorname>Fatal server error: could not open default font
'fixed'</errorname></para>
</question>

<answer>
<para>This question should be obsolete as the default font is now built into the server.</para>

<para>This error occurs for one of three reasons:</para>

<orderedlist spacing="compact">
<listitem>
<para>You do not have a font package which provides the default font
('fixed') installed.  This is
rarely the problem; but in the event that it is the problem, just
rerun &cygwin;' setup program, select the
&package-fnts; package and install it.</para>
</listitem>

<listitem>
<para>The mount point for
<filename>&path-install-fonts;</filename> was either invalid (does 
not point to a valid folder on your system) or is a text-mode mount.
You can confirm that this is the problem by running <command>mount</command> 
from a &cygwin; shell and checking the disk path returned for the
<filename>&path-install-fonts;</filename> mount point.</para>

<note><para>You cannot reliably fix this problem by deleting your
&cygwin; installation and reinstalling it.  The mount points that
&cygwin; was using will be left in your system settings and the
invalid mount point for
<filename>&path-install-fonts;</filename> will be used
again when you perform the reinstallation.  You SHOULD follow the
instructions below to fix the problem.</para></note>

<para>To fix the problem, perform the following steps:</para>

<orderedlist spacing="compact">
<listitem>
<para>Open a &cygwin; shell and run <command>umount
&path-install-fonts;</command>.</para>
</listitem>

<listitem>
<para>Close the &cygwin; shell.</para>
</listitem>

<listitem>
<para>Run &cygwin;'s setup program.</para>
</listitem>

<listitem>
<para>For each of the font packages, if they are marked Keep,
then select Reinstall, otherwise leave them as they are:</para>
</listitem>

<listitem>
<para>Allow &cygwin;'s setup program to download and
reinstall the fonts packages.  The key to fixing this problem is that
the files were previously untarred into an invalid location; removing
the mount point for the fonts directory should result in the files
being untarred to a valid location.</para>
</listitem>

</orderedlist>
</listitem>

<listitem>
<para>You chose "DOS/text" as the "Default Text File type" during &cygwin; setup, ignoring the advice that the Default Text File Type should be left on Unix/binary unless you have a very good reason to switch it to DOS/text.</para>
<para>Open a &cygwin; shell and run.</para>
<screen>
umount &path-install-fonts;
mount -f -s -b "C:/cygwin/usr/share/fonts" "/usr/share/fonts"
</screen>
<para>Reinstall your fonts</para>
</listitem>

</orderedlist>

</answer>
</qandaentry>


<qandaentry>
<question id="q-removing-font-path-element">
<para><errorname>Could not init font path element
&path-install-fonts;<replaceable>*</replaceable>/, removing from
list!</errorname></para>
</question>

<answer>
<para>These warnings are generally harmless since they indicate that
default search paths for fonts do not actually contain fonts; this is
only a problem if the <filename>misc</filename> path does not contain
fonts and/or all of the paths do not contain fonts.</para>

<para>If you are getting these message and the X Server is also
failing to start, then see <xref linkend="q-error-font-eof"></xref>
for information on how to fix your fonts.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-procedure-entry-point-missing">
<para><errorname>The procedure entry point _check_for_executable could
not be located</errorname></para>
</question>

<answer>
<para>This question should be obsolete.</para>
<para>Programs that you are attempting to use were compiled against a
newer version of &cygwin; than is currently on your system.  Run
&cygwin;'s setup program to update your installation to the latest
version.</para>
</answer>
</qandaentry>

<qandaentry>
<question id="cygX11-6.dll-missing">
<para>cygX11-6.dll not found after installation or upgrade</para>
</question>
<answer>
<para>This question should be obsolete.</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-status-access-violation">
<para><errorname>Exception: STATUS_ACCESS_VIOLATION</errorname></para>
</question>

<answer>
<para>
<itemizedlist spacing="compact">
<listitem>
<para>
It is believed that this may have the same underlying causes as <xref linkend="q-fork-failures"></xref>
</para>
</listitem>
<listitem>
<para><filename>&cygwin-dll;</filename> uses a shared memory section
amongst all loaded copies of <filename>&cygwin-dll;</filename>;
unfortunately, the layout and usage of the shared memory section
changes between versions of <filename>&cygwin-dll;</filename>.
Loading two different versions of <filename>&cygwin-dll;</filename>
will cause the shared memory section to become corrupted, which almost
always results in an <errorname>Exception:
STATUS_ACCESS_VIOLATION</errorname>.  You must search your
filesystem(s) and remove all copies of
<filename>&cygwin-dll;</filename> except the copy in <filename
class="directory">/bin</filename>.  You must remove the different
versions of <filename>&cygwin-dll;</filename> even if they are not in
your path, as programs that depend on
<filename>&cygwin-dll;</filename> attempt to load the file from the
local directory before searching other paths; thus, it is rather easy,
and common, for multiple versions of <filename>&cygwin-dll;</filename>
to become loaded at the same time if they exist on a particular
system.</para>
</listitem>
<listitem>
<para>
See also <ulink url="http://cygwin.com/faq.html#faq.setup.setup-fails-on-ts">
this main &cygwin; FAQ question</ulink> for an issue which may cause this
problem with older binaries on Terminal Server.
</para>
</listitem>
</itemizedlist>
</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-error-max-clients">
<para><errorname>Xlib: connection to
"<replaceable>local_host_name_or_ip_address</replaceable>:0.0" refused
by server Xlib: Maximum number of clients reached</errorname></para>
</question>

<answer>
<para>&project; queries getdtablesize() for the maximum number of
client connections allowed; by default &cygwin; returns 32 from
getdtablesize().  &project; <ulink
url="&project-url-devel-shadow;">Server Test Series</ulink> release
<ulink url="&project-url-devel-shadow-changelog;">Test44</ulink>,
released on 2001-08-15, changed the maximum number of clients from 32
to 1024 by passing the square of getdtablesize() to setdtablesize().
</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-xio-fatal-io-error-104">
<para><errorname>XIO: fatal IO error 104 (Connection reset by peer) on
 X server "127.0.0.1:0.0"</errorname></para>
</question>

<answer>
<para>See <xref linkend="q-error-max-clients"></xref></para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-cannot-open-display">
<para><errorname>Cannot Open Display: 127.0.0.1:0.0</errorname></para>
</question>

<answer>
<para>Certain classes of software, such as that used for <glossterm
linkend="gloss-vpn">Virtual Private Networking</glossterm> and
<glossterm linkend="gloss-firewall">fire-walling</glossterm> may cause
the IP address 127.0.0.1, or other local adapter addresses, to be
redirected, to become inoperable in some way, or to be operated in a
manner that violates the defined operation of IP address.</para>

<para>As a potential remedy, try removing all instances of such
software; this may not always fix the problem though, as some software
may leave artifacts even after uninstallation is completed.  The only
way to be sure that you have not found a &project; bug is to install
Windows on a freshly formatted hard drive, followed by &cygwin; and
&project, and finally add your other software one application at a
time until &project; stops working.</para>

<para>Some products that have been reported to cause problems:

<itemizedlist spacing="compact">
<listitem>
<para><application>Aventail Connect</application></para>
</listitem>

<listitem>
<para><application>Zonealarm PC Firewall</application> from Zonelab</para>
</listitem>
</itemizedlist>

<note><para>These products may not cause problems in all configurations.
However, the &project; project has neither the time, ability, nor
resources to help you correctly configure your third-party
software.</para></note>

See <ulink url="http://cygwin.com/faq.html#faq.using.bloda">
the main &cygwin; FAQ question</ulink> for an up-to-date list of software
which has been known to interfere with the correct operation of &cygwin;.
</para>

</answer>
</qandaentry>

<qandaentry>
<question id="q-error-env-space">
<para>Out of environment space</para>
</question>

<answer>
<para>Increase your Windows environment space by following the <ulink
url="http://support.microsoft.com/support/kb/articles/Q230/2/05.ASP">instructions
provided by Microsoft</ulink>.</para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-too-many-parameters">
<para>Too many parameters</para>
</question>

<answer>
<para>See <xref linkend="q-error-env-space"></xref></para>
</answer>
</qandaentry>


<qandaentry>
<question id="q-xcb-lock-assertion">
<para>
"xcb_xlib_lock: Assertion '!c->xlib.lock' failed." or "xcb_xlib_unlock: Assertion 'c->xlib.lock' failed."
</para>
</question>

<answer>
<para>This question should be obsolete.</para>
<para>
Both of these represent bugs in a caller of libX11, and <emphasis>not</emphasis> in libX11
or libxcb. The first assertion means that a caller attempted to lock
the display while already locked. The second assertion means that a
caller attempted to unlock the display without having it locked. 
</para>

<para>
If you encounter such bugs, please report a bug against the offending 
software (which is <emphasis>not</emphasis> libX11 or libxcb)
</para>

<para>
This error can be worked around by using <command>export LIBXCB_ALLOW_SLOPPY_LOCK=1</command>
</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-failed-to-compile-keymap">
<para>Fatal server error: Failed to activate core devices.</para>
</question>

<answer>
<para>
<screen>
(EE) XKB: Could not invoke xkbcomp
(EE) XKB: Couldn't compile keymap
XKB: Failed to compile keymap
Keyboard initialization failed. This could be a missing or incorrect setup of xkeyboard-config.
Fatal server error: Failed to activate core devices.
</screen>

<itemizedlist spacing="compact">
<listitem>
<para>
Verify that xkeyboard-config is correctly installed using <command>cygcheck -c xkeyboard-config</command>,
</para>
</listitem>

<listitem>
<para>
Check that <command>/usr/bin/xkbcomp</command> can be run from a bash shell.  If that fails, see if
<command>cygcheck /usr/bin/xkbcomp</command> reports any missing DLLs.
</para>
</listitem>

<listitem>
<para>
Something is interferring with the ability of the X server to invoke <command>xkbcomp</command>
to compile the keymap.
</para>

<para>
See <xref linkend="q-fork-failures"></xref> for possible causes.
</para>
</listitem>
</itemizedlist>

</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-fork-failures">
<para>"fatal error - unable to remap (some dll) to same address as parent: (some hex number) != (some other hex number)" or
"(some dll): Loaded to different address: parent(some hex number) != child(some other hex number)"</para>
</question>

<answer>
<para>
This is commonly caused by one of three things:
<itemizedlist spacing="compact">
<listitem>
<para>
You have run &cygwin;'s setup program to do an update while some
cygwin processes were running, and then clicked on the continue option in the
"In-use files detected" dialog, and then tried to carry on using &cygwin; without
rebooting as advised by setup.  Reboot.
</para>
</listitem>

<listitem>
<para>
This is one of the symptoms of an application interfering with &cygwin;'s fork() emulation.
See <ulink url="http://cygwin.com/faq.html#faq.using.bloda">
the main &cygwin; FAQ question</ulink> for a list of software
which has been known to interfere with the correct operation of &cygwin;.
</para>
</listitem>

<listitem>
<para>
This also caused by DLLs with conflicting base addresses preventing &cygwin;'s fork() emulation
from functioning correctly.
See <ulink url="http://cygwin.com/faq.html#faq.using.fixing-fork-failures">
the main &cygwin; FAQ question</ulink> for advice.
</para>
</listitem>
</itemizedlist>
</para>
</answer>
</qandaentry>

<qandaentry>
<question id="q-died-waiting-for-longjmp">
<para>fork: child -1 - died waiting for longjmp before initialization, retry (some number), exit code (some hex number), errno (some other number)</para>
</question>

<answer>
<para>
This is believed to have the same underlying causes as <xref linkend="q-fork-failures"></xref>
</para>
</answer>
</qandaentry>

</qandadiv>
