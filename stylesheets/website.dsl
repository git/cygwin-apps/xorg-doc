<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY docbook.dsl 
         PUBLIC "-//Norman Walsh//DOCUMENT DocBook HTML Stylesheet//EN"
         CDATA DSSSL>
]>

<style-sheet>
<style-specification use="docbook">
<style-specification-body>

;; (much of these are from from cygnus-both.dsl)

;; this is necessary because right now jadetex does not understand
;; symbolic entities, whereas things work well with numeric entities.
(declare-characteristic preserve-sdata?
          "UNREGISTERED::James Clark//Characteristic::preserve-sdata?"
          #f)

;; put the legal notice in a separate file
;;(define %generate-legalnotice-link%
;;  #t)

;; make funcsynopsis look pretty
(define %funcsynopsis-decoration%
  ;; Decorate elements of a FuncSynopsis?
  #t)

(define %html-ext% ".html")

(define %html-header-tags% 
  '(("META" ("HTTP-EQUIV" "Content-Type") ("CONTENT" "text/html;charset=utf-8"))))

(define %body-attr%
  ;; What attributes should be hung off of BODY?
  '())
;;  (list
;;   (list "BGCOLOR" "#FFFFFF")
;;   (list "TEXT" "#000000")))

(define %generate-article-toc%
  ;; Should a Table of Contents be produced for Articles?
  ;; If true, a Table of Contents will be generated for each 'Article'.
  #t)

(define %generate-part-toc% #t)

(define %shade-verbatim%
  #t)

(define %use-id-as-filename%
  ;; Use ID attributes as name for component HTML files?
  #t)

(define %graphic-default-extension% "gif")

;; use the cygxfree website css stylesheet
;;(define %stylesheet%
  ;; Name of the stylesheet to use
  ;;"../xfree86.css")

;; the website is 4.0 transitional
(define %html-pubid%
  ;; What public ID are you declaring your HTML compliant with?
  "-//W3C//DTD HTML 4.0 Transitional//EN")

(define %html40%
  ;; Generate HTML 4.0
  #t)

(define %fix-para-wrappers%
  ;; Block element in para hack
  #f)

(define %spacing-paras%
  ;; Block-element spacing hack
  #f)

</style-specification-body>
</style-specification>

<external-specification id="docbook" document="docbook.dsl">

</style-sheet>

